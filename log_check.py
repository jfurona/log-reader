#!/usr/bin/python

##    Script to parse IPs from logs and look up their whois information.
##    Copyright (C) 2015  Justin Farone - justin@farone.us
##
##    This program is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.


try:
    import sys
    import threading
    import time
    import re
    from ipwhois import IPWhois
    from ipwhois.utils import get_countries
    from tailf import tailf
except:
    print "Please make sure IPWhois and pytailf are installed."
    exit(1)

if len(sys.argv) != 4:
    print "Incorrect number of arguments\nProper usage: %s /full/log/path.log lines threshold" % (sys.argv[0], )
    exit(2)

p=re.compile('(?:[0-9]{1,3}\.){3}[0-9]{1,3}')
t_pid = 1
counter = 0
record = dict()
countries = get_countries()
log_path = sys.argv[1].strip()

try:
    logFile = open(log_path, "r")
except:
    print "Log file does not exist"
    exit(5)
else:
    logFile.close()

try:
    parse_recs = int(sys.argv[2])
except:
    print "Number of lines to read must be an integer."
    exit(3)
    
try:
    max_attempt = int(sys.argv[3])
except:
    print "Threshold must be an integer."
    exit(4)

class myThread (threading.Thread):
    def __init__(self, threadID, line):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.line = line
    def run(self):
        currip = get_ip(self.line)
        collect_ip(currip)
        
def get_ip(line):
    spline = line.split()
    for word in spline:
        ip = p.match(word)
        if ip:
            return ip.group()

def collect_ip(currip):
    if currip in record:
        record[currip] += 1
    else:
        record[currip] = 1

for line in tailf(log_path):
    thread = myThread(t_pid, line)
    t_pid +=1 
    thread.start()
    counter +=1
    if counter >= parse_recs:
        break

for (key,val) in record.items():
    if val > max_attempt:
        whois_obj = IPWhois(key)
        whois_res = whois_obj.lookup()
        whois_country = countries[whois_res['asn_country_code']]
        whois_name = whois_res ['nets'][0]['name']
        whois_desc = whois_res ['nets'][0]['description']
        print "%s visited %i times from %s in %s - %s" % (key,val,whois_name, whois_country, whois_desc)

